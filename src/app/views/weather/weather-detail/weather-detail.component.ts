import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { WeatherInterface } from 'src/app/Interfaces/weather-interface';
import { weatherSelector } from 'src/app/stores/weather.selector';
import * as weatherActions from 'src/app/stores/weather.actions';

@Component({
  selector: 'app-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.scss'],
})
export class WeatherDetailComponent {
  weather$: Observable<WeatherInterface | null>;

  constructor(private store: Store) {
    this.weather$ = this.store.pipe(select(weatherSelector));
  }

  setCityName(value: string | null) {
    if (value !== '' && value !== null) {
      this.store.dispatch(weatherActions.getWeather({ countryName: value }));
    }
  }
}
