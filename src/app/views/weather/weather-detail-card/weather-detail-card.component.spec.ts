import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherDetailCardComponent } from './weather-detail-card.component';

describe('WeatherDetailCardComponent', () => {
  let component: WeatherDetailCardComponent;
  let fixture: ComponentFixture<WeatherDetailCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WeatherDetailCardComponent]
    });
    fixture = TestBed.createComponent(WeatherDetailCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
