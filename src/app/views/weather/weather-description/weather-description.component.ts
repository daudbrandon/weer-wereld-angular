import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, map } from 'rxjs';
import { Current } from 'src/app/Interfaces/current';
import { Hour } from 'src/app/Interfaces/forecast';
import {
  currentWeatherSelector,
  weatherHoursSelector,
} from 'src/app/stores/weather.selector';

@Component({
  selector: 'app-weather-description',
  templateUrl: './weather-description.component.html',
  styleUrls: ['./weather-description.component.scss'],
})
export class WeatherDescriptionComponent {
  currentWeather$: Observable<Current | null>;
  weatherHourList$: Observable<Hour[]>;
  weatherDetails: {
    title: string;
    detail: string | null;
    footer: string | null;
  }[] = [];
  currentTimestamp = Math.floor(new Date().getTime() / 1000);

  constructor(private store: Store) {
    this.currentWeather$ = this.store.select(currentWeatherSelector);
    this.weatherHourList$ = this.store
      .select(weatherHoursSelector)
      .pipe(
        map((hours: Hour[]) =>
          hours.filter((hour) => hour.time_epoch > this.currentTimestamp)
        )
      );
    this.setForecastDetailList();
  }

  setForecastDetailList() {
    this.currentWeather$.subscribe((currentWeather) => {
      if (currentWeather !== null) {
        this.weatherDetails = [
          {
            title: 'Wind',
            detail: currentWeather?.wind_kph + ' km/h',
            footer: currentWeather?.wind_dir,
          },
          {
            title: 'Humidity',
            detail: currentWeather?.humidity.toString(),
            footer: null,
          },
          {
            title: 'Real Feel',
            detail: currentWeather?.feelslike_c.toString() + 'ºC',
            footer: null,
          },
          {
            title: 'Uv Index',
            detail: currentWeather?.uv.toString(),
            footer: 'Moderate',
          },
          {
            title: 'Pressure',
            detail: currentWeather?.pressure_mb.toString(),
            footer: null,
          },
        ];
      }
    });
  }
}
