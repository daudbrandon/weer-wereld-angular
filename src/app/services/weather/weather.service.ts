import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WeatherInterface } from 'src/app/Interfaces/weather-interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WeatherService {
  protected apiKey: string = environment.weatherApiKey;
  protected apiUrl: string = environment.forecastWeatherApiUrl;

  constructor(private http: HttpClient) {}

  getData(countryName: string): Observable<WeatherInterface> {
    return this.http.get<WeatherInterface>(
      this.apiUrl + '?key=' + this.apiKey + '&q=' + countryName + '&days=10'
    );
  }
}
