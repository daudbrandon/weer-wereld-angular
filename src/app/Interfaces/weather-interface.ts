import { Current } from './current';
import { Forecast } from './forecast';
import { Location } from './location';

export interface WeatherInterface {
  location: Location;
  current: Current;
  forecast: Forecast;
}
