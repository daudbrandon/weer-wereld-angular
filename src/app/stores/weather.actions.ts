import { createAction, props } from '@ngrx/store';
import { WeatherInterface } from '../Interfaces/weather-interface';

export const getWeather = createAction(
  '[Weather] get weather',
  props<{ countryName: string }>()
);

export const getWeatherSuccess = createAction(
  '[Weather] get weather success',
  props<WeatherInterface>()
);
export const getWeatherError = createAction(
  '[Weather] get weather error',
  props<any>()
);
