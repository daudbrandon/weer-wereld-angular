import { Component, ElementRef, Input, Renderer2 } from '@angular/core';
import { Forecast, Forecastday } from 'src/app/Interfaces/forecast';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss'],
})
export class ForecastComponent {
  @Input() forecastDay!: Forecastday;
  isActive: boolean = false;

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  toggleActive() {
    const forecastListHeight =
      this.el.nativeElement.querySelector('.forecast-hourly-list-wrapper')
        .offsetHeight * 2;

    const wrapper = this.el.nativeElement.querySelector(
      '.forecast-hourly-wrapper'
    );

    if (this.isActive) {
      this.renderer.setStyle(wrapper, 'height', '0px');
    } else {
      this.renderer.setStyle(wrapper, 'height', forecastListHeight + 'px');
      this.renderer.setStyle(wrapper, 'max-height', '30vh');
    }

    this.isActive = !this.isActive;
  }
}
