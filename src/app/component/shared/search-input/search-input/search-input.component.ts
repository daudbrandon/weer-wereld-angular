import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import {  debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent {
  @Input() placeholderValue: string = '';
  @Output() handleCountryNameChange = new EventEmitter<string | null>();

  countryName = new FormControl('');
  changeNameTimeout: any;

  constructor() {
    this.countryName.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.handleCountryNameChange.emit(value);
      });
  }
}
