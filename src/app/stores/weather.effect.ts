import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as weatherActions from './weather.actions';
import { catchError, map, mergeMap, of } from 'rxjs';
import { WeatherService } from '../services/weather/weather.service';
import { Injectable } from '@angular/core';
import { LocationService } from '../services/location/location.service';

@Injectable()
export class WeatherEffect {
  loadWeatherData$ = createEffect(() =>
    this.action$.pipe(
      ofType(weatherActions.getWeather),
      mergeMap((action) =>
        this.weatherService.getData(action.countryName).pipe(
          map((weatherData) => {
            this.locationService.setIsDataShowPermitted(true);
            return weatherActions.getWeatherSuccess(weatherData);
          }),
          catchError((error) => of(weatherActions.getWeatherError(error)))
        )
      )
    )
  );

  constructor(
    private action$: Actions,
    private weatherService: WeatherService,
    private locationService: LocationService
  ) {}
}
