import { WeatherInterface } from "./weather-interface";


export interface WeatherStateInterface {
  data: WeatherInterface | null;
  isLoading: boolean;
  error: any;
}
