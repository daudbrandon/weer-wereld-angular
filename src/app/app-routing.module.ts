import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import { HomeModule } from './views/home/home.module';
import { WeatherModule } from './views/weather/weather.module';
import { WeatherComponent } from './views/weather/weather.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'weather',
    loadChildren: () =>
      import('./views/weather/weather.module').then((m) => m.WeatherModule),
  },
];
@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(appRoutes)],
  exports: [RouterModule, HomeModule],
})
export class AppRoutingModule {}
