import { Injectable, signal } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LocationService {
  private locationSubject = new BehaviorSubject<{
    lat: number;
    long: number;
  } | null>(null);

  private isPermitted: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  constructor() {}

  public isDataShowPermitted(): BehaviorSubject<boolean> {
    return this.isPermitted;
  }

  public setIsDataShowPermitted(value: boolean) {
    this.isPermitted.next(value);
  }

  public getLocation(): Promise<{ lat: number; long: number }> {
    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          const location = {
            lat: position.coords.latitude,
            long: position.coords.longitude,
          };
          this.setIsDataShowPermitted(true);

          resolve(location);
        });
      } else {
        this.setIsDataShowPermitted(false);
      }
    });
  }
}
