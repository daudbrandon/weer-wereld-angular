import { createFeatureSelector, createSelector, on } from '@ngrx/store';
import { WeatherStateInterface } from '../Interfaces/weather-state-interface';

export const selectFeature =
  createFeatureSelector<WeatherStateInterface>('Weather');

export const weatherSelector = createSelector(
  selectFeature,
  (state) => state.data
);

export const weatherIsLoadingSelector = createSelector(
  selectFeature,
  (state) => state.isLoading
);
export const weatherErrorSelector = createSelector(
  selectFeature,
  (state) => state.error
);

export const currentWeatherSelector = createSelector(
  selectFeature,
  (state) => state.data?.current ?? null
);

export const weatherHoursSelector = createSelector(
  selectFeature,
  (state) => state.data?.forecast.forecastday[0].hour ?? []
);

export const forecastWeathyerDayListSelector = createSelector(
  selectFeature,
  (state) => state.data?.forecast.forecastday ?? []
);
