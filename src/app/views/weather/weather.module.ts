// weather.module.ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherDescriptionComponent } from './weather-description/weather-description.component';
import { WeatherDetailCardComponent } from './weather-detail-card/weather-detail-card.component';
import { WeatherDetailComponent } from './weather-detail/weather-detail.component';
import { ForecastComponent } from './forecast/forecast.component';
import { WeatherComponent } from './weather.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule, Routes } from '@angular/router';
import { SearchInputComponent } from 'src/app/component/shared/search-input/search-input/search-input.component';
import { ReactiveFormsModule } from '@angular/forms';
import { WeatherEffect } from 'src/app/stores/weather.effect';
import { weatherReducer } from 'src/app/stores/weather.reducer';
import { LoaderComponent } from 'src/app/component/shared/loader/loader.component';

const routes: Routes = [
  {
    path: '',
    component: WeatherComponent,
  },
];

@NgModule({
  declarations: [
    WeatherDescriptionComponent,
    WeatherDetailCardComponent,
    WeatherDetailComponent,
    ForecastComponent,
    SearchInputComponent,
    WeatherComponent,
    LoaderComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('Weather', weatherReducer),
    EffectsModule.forFeature([WeatherEffect]),
    RouterModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    WeatherDescriptionComponent,
    WeatherDetailCardComponent,
    WeatherDetailComponent,
    ForecastComponent,
    SearchInputComponent,
    LoaderComponent,
  ],
})
export class WeatherModule {}
