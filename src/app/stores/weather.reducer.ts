import { createReducer, on } from '@ngrx/store';
import { WeatherStateInterface } from '../Interfaces/weather-state-interface';
import * as weatherActions from './weather.actions';

export const weatherInitialState: WeatherStateInterface = {
  data: null,
  isLoading: true,
  error: null,
};

export const weatherReducer = createReducer(
  weatherInitialState,
  on(weatherActions.getWeather, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(weatherActions.getWeatherSuccess, (state, newWeatherData) => ({
    ...state,
    data: newWeatherData,
    isLoading: false,
  })),
  on(weatherActions.getWeather, (state, error) => ({
    ...state,
    isLoading: true,
    error: error,
  }))
);
