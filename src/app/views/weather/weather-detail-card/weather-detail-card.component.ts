import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-weather-detail-card',
  templateUrl: './weather-detail-card.component.html',
  styleUrls: ['./weather-detail-card.component.scss'],
})
export class WeatherDetailCardComponent {
  @Input() title: string = '';
  @Input() detail: string | null= '';
  @Input() footer: string | null= '';
}
