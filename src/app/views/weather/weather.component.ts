import { Component, Input, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { BehaviorSubject, Observable, map, switchMap } from 'rxjs';
import { WeatherInterface } from 'src/app/Interfaces/weather-interface';
import { Store, select } from '@ngrx/store';
import {
  forecastWeathyerDayListSelector,
  weatherErrorSelector,
  weatherIsLoadingSelector,
  weatherSelector,
} from 'src/app/stores/weather.selector';
import * as weatherActions from 'src/app/stores/weather.actions';
import { Forecastday } from 'src/app/Interfaces/forecast';
import { LocationService } from 'src/app/services/location/location.service';

const enterTransition = transition(':enter', [
  style({
    opacity: 0,
    height: 0,
    overflow: 'hidden',
  }),
  animate('.5s ease-in-out', style({ opacity: 1 })),
]);

const fadeIn = trigger('fadeIn', [enterTransition]);

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  animations: [fadeIn],
})
export class WeatherComponent implements OnInit {
  isLoading$: Observable<boolean>;
  error$: Observable<any>;
  weather$: Observable<WeatherInterface | null>;
  forecastWeathersDayList$: Observable<Forecastday[]>;
  isLocationPermitted: boolean = false;

  isTodayActive: boolean = true;

  toggleActive(value: boolean) {
    this.isTodayActive = value;
  }

  constructor(private store: Store, private locationService: LocationService) {
    const currentDateEpoch = Math.floor(new Date().getTime() / 1000);
    this.isLoading$ = store.pipe(select(weatherIsLoadingSelector));
    this.weather$ = store.pipe(select(weatherSelector));
    this.error$ = store.pipe(select(weatherErrorSelector));
    this.forecastWeathersDayList$ = this.store
      .pipe(select(forecastWeathyerDayListSelector))
      .pipe(
        map((forecastWeathersDays: Forecastday[]) => {
          return forecastWeathersDays.filter(
            (day) => day.date_epoch > currentDateEpoch
          );
        })
      );
    this.locationService.isDataShowPermitted().subscribe((isPermitted) => {
      this.isLocationPermitted = isPermitted;
    });
  }

  ngOnInit() {
    this.locationService.getLocation().then((location) => {
      if (location) {
        const locationByLatAndLang = location.lat + ',' + location.long;

        this.store.dispatch(
          weatherActions.getWeather({ countryName: locationByLatAndLang })
        );
      }
    });
  }
}
