import { FORECAST_API_KEY, FORECAST_URL } from 'env';
export const environment = {
  production: true,
  forecastWeatherApiUrl: FORECAST_URL,
  weatherApiKey: FORECAST_API_KEY,
};
